﻿namespace ProgrammingProject2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flavorsListBox = new System.Windows.Forms.ListBox();
            this.flavorPromptLabel = new System.Windows.Forms.Label();
            this.cupRadioButton = new System.Windows.Forms.RadioButton();
            this.cupOrConeBox = new System.Windows.Forms.GroupBox();
            this.coneRadioButton = new System.Windows.Forms.RadioButton();
            this.toppingsBox = new System.Windows.Forms.GroupBox();
            this.whippedCreamCheckBox = new System.Windows.Forms.CheckBox();
            this.peanutsCheckBox = new System.Windows.Forms.CheckBox();
            this.chocolateSauceCheckBox = new System.Windows.Forms.CheckBox();
            this.orderPromptLabel = new System.Windows.Forms.Label();
            this.placeOrderButton = new System.Windows.Forms.Button();
            this.finalOrderLabel = new System.Windows.Forms.Label();
            this.outputLabel = new System.Windows.Forms.Label();
            this.clearButton = new System.Windows.Forms.Button();
            this.cupOrConeBox.SuspendLayout();
            this.toppingsBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // flavorsListBox
            // 
            this.flavorsListBox.FormattingEnabled = true;
            this.flavorsListBox.Items.AddRange(new object[] {
            "Vanilla",
            "Chocolate",
            "Strawberry",
            "Mint Chocolate Chip"});
            this.flavorsListBox.Location = new System.Drawing.Point(36, 97);
            this.flavorsListBox.Name = "flavorsListBox";
            this.flavorsListBox.Size = new System.Drawing.Size(147, 82);
            this.flavorsListBox.TabIndex = 0;
            // 
            // flavorPromptLabel
            // 
            this.flavorPromptLabel.AutoSize = true;
            this.flavorPromptLabel.Location = new System.Drawing.Point(33, 81);
            this.flavorPromptLabel.Name = "flavorPromptLabel";
            this.flavorPromptLabel.Size = new System.Drawing.Size(81, 13);
            this.flavorPromptLabel.TabIndex = 1;
            this.flavorPromptLabel.Text = "Select a Flavor:";
            // 
            // cupRadioButton
            // 
            this.cupRadioButton.AutoSize = true;
            this.cupRadioButton.Location = new System.Drawing.Point(6, 19);
            this.cupRadioButton.Name = "cupRadioButton";
            this.cupRadioButton.Size = new System.Drawing.Size(44, 17);
            this.cupRadioButton.TabIndex = 3;
            this.cupRadioButton.Text = "Cup";
            this.cupRadioButton.UseVisualStyleBackColor = true;
            // 
            // cupOrConeBox
            // 
            this.cupOrConeBox.Controls.Add(this.coneRadioButton);
            this.cupOrConeBox.Controls.Add(this.cupRadioButton);
            this.cupOrConeBox.Location = new System.Drawing.Point(270, 81);
            this.cupOrConeBox.Name = "cupOrConeBox";
            this.cupOrConeBox.Size = new System.Drawing.Size(144, 94);
            this.cupOrConeBox.TabIndex = 4;
            this.cupOrConeBox.TabStop = false;
            this.cupOrConeBox.Text = "Cup or Cone?";
            // 
            // coneRadioButton
            // 
            this.coneRadioButton.AutoSize = true;
            this.coneRadioButton.Location = new System.Drawing.Point(6, 42);
            this.coneRadioButton.Name = "coneRadioButton";
            this.coneRadioButton.Size = new System.Drawing.Size(50, 17);
            this.coneRadioButton.TabIndex = 4;
            this.coneRadioButton.TabStop = true;
            this.coneRadioButton.Text = "Cone";
            this.coneRadioButton.UseVisualStyleBackColor = true;
            // 
            // toppingsBox
            // 
            this.toppingsBox.Controls.Add(this.whippedCreamCheckBox);
            this.toppingsBox.Controls.Add(this.peanutsCheckBox);
            this.toppingsBox.Controls.Add(this.chocolateSauceCheckBox);
            this.toppingsBox.Location = new System.Drawing.Point(487, 81);
            this.toppingsBox.Name = "toppingsBox";
            this.toppingsBox.Size = new System.Drawing.Size(144, 94);
            this.toppingsBox.TabIndex = 5;
            this.toppingsBox.TabStop = false;
            this.toppingsBox.Text = "Choose your toppings:";
            // 
            // whippedCreamCheckBox
            // 
            this.whippedCreamCheckBox.AutoSize = true;
            this.whippedCreamCheckBox.Location = new System.Drawing.Point(3, 62);
            this.whippedCreamCheckBox.Name = "whippedCreamCheckBox";
            this.whippedCreamCheckBox.Size = new System.Drawing.Size(102, 17);
            this.whippedCreamCheckBox.TabIndex = 2;
            this.whippedCreamCheckBox.Text = "Whipped Cream";
            this.whippedCreamCheckBox.UseVisualStyleBackColor = true;
            // 
            // peanutsCheckBox
            // 
            this.peanutsCheckBox.AutoSize = true;
            this.peanutsCheckBox.Location = new System.Drawing.Point(3, 39);
            this.peanutsCheckBox.Name = "peanutsCheckBox";
            this.peanutsCheckBox.Size = new System.Drawing.Size(65, 17);
            this.peanutsCheckBox.TabIndex = 1;
            this.peanutsCheckBox.Text = "Peanuts";
            this.peanutsCheckBox.UseVisualStyleBackColor = true;
            // 
            // chocolateSauceCheckBox
            // 
            this.chocolateSauceCheckBox.AutoSize = true;
            this.chocolateSauceCheckBox.Location = new System.Drawing.Point(3, 16);
            this.chocolateSauceCheckBox.Name = "chocolateSauceCheckBox";
            this.chocolateSauceCheckBox.Size = new System.Drawing.Size(108, 17);
            this.chocolateSauceCheckBox.TabIndex = 0;
            this.chocolateSauceCheckBox.Text = "Chocolate Sauce";
            this.chocolateSauceCheckBox.UseVisualStyleBackColor = true;
            // 
            // orderPromptLabel
            // 
            this.orderPromptLabel.AutoSize = true;
            this.orderPromptLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderPromptLabel.Location = new System.Drawing.Point(348, 43);
            this.orderPromptLabel.Name = "orderPromptLabel";
            this.orderPromptLabel.Size = new System.Drawing.Size(147, 16);
            this.orderPromptLabel.TabIndex = 6;
            this.orderPromptLabel.Text = "Order An Ice Cream!";
            // 
            // placeOrderButton
            // 
            this.placeOrderButton.Location = new System.Drawing.Point(709, 81);
            this.placeOrderButton.Name = "placeOrderButton";
            this.placeOrderButton.Size = new System.Drawing.Size(99, 94);
            this.placeOrderButton.TabIndex = 7;
            this.placeOrderButton.Text = "Place My Order";
            this.placeOrderButton.UseVisualStyleBackColor = true;
            this.placeOrderButton.Click += new System.EventHandler(this.placeOrderButton_Click);
            // 
            // finalOrderLabel
            // 
            this.finalOrderLabel.AutoSize = true;
            this.finalOrderLabel.Location = new System.Drawing.Point(33, 249);
            this.finalOrderLabel.Name = "finalOrderLabel";
            this.finalOrderLabel.Size = new System.Drawing.Size(94, 13);
            this.finalOrderLabel.TabIndex = 8;
            this.finalOrderLabel.Text = "Here\'s Your Order:";
            // 
            // outputLabel
            // 
            this.outputLabel.Location = new System.Drawing.Point(133, 248);
            this.outputLabel.Name = "outputLabel";
            this.outputLabel.Size = new System.Drawing.Size(675, 23);
            this.outputLabel.TabIndex = 11;
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(384, 315);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(75, 23);
            this.clearButton.TabIndex = 12;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 394);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.outputLabel);
            this.Controls.Add(this.finalOrderLabel);
            this.Controls.Add(this.placeOrderButton);
            this.Controls.Add(this.orderPromptLabel);
            this.Controls.Add(this.toppingsBox);
            this.Controls.Add(this.cupOrConeBox);
            this.Controls.Add(this.flavorPromptLabel);
            this.Controls.Add(this.flavorsListBox);
            this.Name = "Form1";
            this.Text = "Ice Cream Order";
            this.cupOrConeBox.ResumeLayout(false);
            this.cupOrConeBox.PerformLayout();
            this.toppingsBox.ResumeLayout(false);
            this.toppingsBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox flavorsListBox;
        private System.Windows.Forms.Label flavorPromptLabel;
        private System.Windows.Forms.RadioButton cupRadioButton;
        private System.Windows.Forms.GroupBox cupOrConeBox;
        private System.Windows.Forms.RadioButton coneRadioButton;
        private System.Windows.Forms.GroupBox toppingsBox;
        private System.Windows.Forms.CheckBox whippedCreamCheckBox;
        private System.Windows.Forms.CheckBox peanutsCheckBox;
        private System.Windows.Forms.CheckBox chocolateSauceCheckBox;
        private System.Windows.Forms.Label orderPromptLabel;
        private System.Windows.Forms.Button placeOrderButton;
        private System.Windows.Forms.Label finalOrderLabel;
        private System.Windows.Forms.Label outputLabel;
        private System.Windows.Forms.Button clearButton;
    }
}

