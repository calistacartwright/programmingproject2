﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgrammingProject2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void placeOrderButton_Click(object sender, EventArgs e)
        {
            //When the placeOrderButton is clicked, the output is determined by what options are selected for the flavor, container, and toppings.
            string flavor = "";
            string container = "";
            string toppings = "";

            //The flavorListBox allows the user to select one type of ice cream, and the name of the selected flavor is converted into a string.
            if (flavorsListBox.SelectedIndex != -1) {
                flavor = flavorsListBox.SelectedItem.ToString();
            }

            //If the cupRadioButton is checked, then the output for the order is the string " in a cup".
            if (cupRadioButton.Checked) {
                container = " in a cup";
            }
            //If the coneRadioButton is checked, then the output for the order is the string " in a cone".
            if (coneRadioButton.Checked) {
                container = " in a cone";
            }

            //Each possible combination of toppings are checked and the output is determined by which combination is selected.
            if (chocolateSauceCheckBox.Checked && peanutsCheckBox.Checked && whippedCreamCheckBox.Checked)
            {
                toppings = " with chocolate sauce, whipped cream, and peanuts";
            }
            else if ((chocolateSauceCheckBox.Checked && peanutsCheckBox.Checked) || (chocolateSauceCheckBox.Checked && whippedCreamCheckBox.Checked) || (whippedCreamCheckBox.Checked && peanutsCheckBox.Checked))
            {
                if (chocolateSauceCheckBox.Checked && peanutsCheckBox.Checked)
                {
                    toppings = " with chocolate sauce and peanuts";
                }
                else if (chocolateSauceCheckBox.Checked && whippedCreamCheckBox.Checked)
                {
                    toppings = " with chocolate sauce and whipped cream";
                }
                else if (whippedCreamCheckBox.Checked && peanutsCheckBox.Checked)
                {
                    toppings = " with whipped cream and peanuts";
                }
            }
            else if (chocolateSauceCheckBox.Checked || peanutsCheckBox.Checked || whippedCreamCheckBox.Checked)
            {
                if (chocolateSauceCheckBox.Checked)
                {
                    toppings = " with chocolate sauce";
                }
                else if (whippedCreamCheckBox.Checked)
                {
                    toppings = " with whipped cream";
                }
                else if (peanutsCheckBox.Checked)
                {
                    toppings = " with peanuts";
                }
            }
            else {
                toppings = " with no toppings";
            }


            //The outputLabel displays the final ice cream, with each part of the order concatenated together.
            outputLabel.Text = flavor + container + toppings;
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            //A button is used to clear all output and unselect all options. This is useful because it would allow the user to easily restart the form.
            outputLabel.Text = "";
            flavorsListBox.SelectedIndex = -1;
            coneRadioButton.Checked = false;
            cupRadioButton.Checked = false;
            chocolateSauceCheckBox.Checked = false;
            peanutsCheckBox.Checked = false;
            whippedCreamCheckBox.Checked = false;
        }
    }
}
